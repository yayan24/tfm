import pandas, argparse

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_DATASET_FILE = '/home/brayan/Taverna/tmpDataset'
TMP_HEADER_FILE = '/home/brayan/Taverna/tmpHeader'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Column drop

parser = argparse.ArgumentParser(description = 'This programdrops a column of the processed dataset.')
parser.add_argument('column', help = 'Name of the column to be dropped')

droppedColumn = parser.parse_args().column

dataset = pandas.read_csv(TMP_DATASET_FILE)
dataset = dataset.drop([droppedColumn], axis = 1)
dataset.to_csv(TMP_DATASET_FILE, encoding = 'utf-8', index = False)

with open(TMP_HEADER_FILE, 'w') as headerFile:
    headerFile.write(','.join(dataset.columns))

print 'Dataset processed correctly!'