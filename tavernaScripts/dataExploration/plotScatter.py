import pandas, argparse
import matplotlib.pyplot as pyplot

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_DATASET_FILE = '/home/brayan/Taverna/tmpDataset'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Plotting

parser = argparse.ArgumentParser(description = 'This program plots a scatter given the colums for the X and Y axis. Dots are coloured by the value of the third column.')
parser.add_argument('columnX', help = 'Name of the column to be plotted as the X axis')
parser.add_argument('columnY', help = 'Name of the column to be plotted as the Y axis')
parser.add_argument('columnObj', help = 'Name of the column to be plotted')

selectedXColumn = parser.parse_args().columnX
selectedYColumn = parser.parse_args().columnY
objColumn = parser.parse_args().columnObj
dataset = pandas.read_csv(TMP_DATASET_FILE)

try:
	dataset.plot(kind = 'scatter', 
		x = selectedXColumn, 
		y = selectedYColumn, 
		c = objColumn, 
		s = 50)
	pyplot.xlabel(selectedXColumn)
	pyplot.ylabel(selectedYColumn)
	pyplot.title(selectedXColumn + ' - ' + selectedYColumn)
	pyplot.grid(True)
	pyplot.show()
except Exception as e:
	print 'Unexpected error:', str(e)

print 'Dataset processed correctly!'