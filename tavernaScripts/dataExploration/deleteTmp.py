import os

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_DATASET_FILE = '/home/brayan/Taverna/tmpDataset'
TMP_HEADER_FILE = '/home/brayan/Taverna/tmpHeader'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Main program

try:
    os.remove(TMP_DATASET_FILE)
    os.remove(TMP_HEADER_FILE)
except OSError:
    pass