import pandas, argparse

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_DATASET_FILE = '/home/brayan/Taverna/tmpDataset'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Missing values processing

parser = argparse.ArgumentParser(description = 'This program sets the given column to a numerical order.')
parser.add_argument('column', help = 'Name of the column to be processed')

classColumn = parser.parse_args().column

dataset = pandas.read_csv(TMP_DATASET_FILE)
classes = dataset[classColumn].unique()

for i in range(len(classes)):
	dataset.loc[dataset[classColumn] == classes[i], classColumn] = i

dataset.to_csv(TMP_DATASET_FILE, encoding = 'utf-8', index = False)

print 'Dataset processed correctly!'