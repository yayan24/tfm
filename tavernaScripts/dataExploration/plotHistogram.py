import pandas, argparse
import matplotlib.pyplot as pyplot

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_DATASET_FILE = '/home/brayan/Taverna/tmpDataset'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Plotting

parser = argparse.ArgumentParser(description = 'This program plots the histogram of the given column')
parser.add_argument('column', help = 'Name of the column to be plotted')

selectedColumn = parser.parse_args().column
dataset = pandas.read_csv(TMP_DATASET_FILE)

try:
	dataset[selectedColumn].dropna().plot(kind = 'hist', 
		xticks  = dataset[selectedColumn].dropna().unique(), 
		bins = dataset[selectedColumn].dropna().unique().size)
	pyplot.xlabel(selectedColumn)
	pyplot.title(selectedColumn + ' distribution')
	pyplot.grid(True)
	pyplot.show()
except Exception as e:
	print 'Unexpected error:', str(e)

print 'Dataset processed correctly!'