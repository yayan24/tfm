import pandas, argparse

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_DATASET_FILE = '/home/brayan/Taverna/tmpDataset'
NEW_FILE_PATH = '/home/brayan/Taverna/'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Main program

parser = argparse.ArgumentParser(description = 'This program saves the loaded dataset in a new file.')
parser.add_argument('fileName', help = 'Name of the new file')

fileName = parser.parse_args().fileName

dataset = pandas.read_csv(TMP_DATASET_FILE)
dataset.to_csv(NEW_FILE_PATH + fileName, encoding = 'utf-8', index = False)

print 'Dataset saved correctly at', NEW_FILE_PATH