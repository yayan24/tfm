import pandas, argparse

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_DATASET_FILE = '/home/brayan/Taverna/tmpDataset'
TMP_HEADER_FILE = '/home/brayan/Taverna/tmpHeader'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Main program

parser = argparse.ArgumentParser(description = 'This module preloads the dataset in auxiliare files for the execution.')
parser.add_argument('filename', help = 'Name of the file to be loaded')
parser.add_argument('--header', help = 'If indicated, the file has a header row', action = 'store_true')

fileName = parser.parse_args().filename
header = parser.parse_args().header

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Data read

if header:
	dataset = pandas.read_csv(fileName)
else:
	dataset = pandas.read_csv(fileName, header = None)
	dataset.columns = map(str, range(dataset.shape[1])) # New header of ints as str
	
dataset = dataset.apply(pandas.to_numeric, errors = 'coerce')

dataset.to_csv(TMP_DATASET_FILE, encoding = 'utf-8', index = False)

with open(TMP_HEADER_FILE, 'w') as headerFile:
    headerFile.write(','.join(dataset.columns))