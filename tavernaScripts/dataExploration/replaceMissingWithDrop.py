import pandas

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_DATASET_FILE = '/home/brayan/Taverna/tmpDataset'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Missing values processing

dataset = pandas.read_csv(TMP_DATASET_FILE)
dataset.dropna(inplace = True)
dataset.to_csv(TMP_DATASET_FILE, encoding = 'utf-8', index = False)

print 'Dataset processed correctly!'