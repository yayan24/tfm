import pandas

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_DATASET_FILE = '/home/brayan/Taverna/tmpDataset'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Normalization

dataset = pandas.read_csv(TMP_DATASET_FILE)
processedDataset = (dataset - dataset.min()) / (dataset.max() - dataset.min())
processedDataset.to_csv(TMP_DATASET_FILE, encoding = 'utf-8', index = False)

print 'Dataset processed correctly!'