import argparse, pandas

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constants definition

TMP_HEADER = '/home/brayan/Taverna/tmpHeaderRec'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Input data read

parser = argparse.ArgumentParser(description = 'This reads the header of the dataset.')
parser.add_argument('filename', help = 'Name of the CSV file that stores the samples')
parser.add_argument('--header', help = 'If indicated, the file has a header row', action = 'store_true')

fileName = parser.parse_args().filename
header = parser.parse_args().header

if header:
	dataset = pandas.read_csv(fileName)
else:
	dataset = pandas.read_csv(fileName, header = None)
	dataset.columns = map(str, range(dataset.shape[1])) # New header of ints as str

with open(TMP_HEADER, 'w') as headerFile:
    headerFile.write(','.join(dataset.columns))