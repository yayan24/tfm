import argparse, pandas, tensorflow, numpy, csv

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constants definition

TMP_VARIABLES_MAP = '/home/brayan/Taverna/tmpVariablesMap'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Input data read

parser = argparse.ArgumentParser(description = 'This program reuses a trained model to give predictions on a selected samples file')
parser.add_argument('filename', help = 'Name of the CSV file that stores the samples, if there are class labels they should be consecutive numbers beginning by 0')
parser.add_argument('weights', help = 'Location of the weights file')
parser.add_argument('biases', help = 'Location of the biases file')
parser.add_argument('--labelColumn', help = 'If the dataset is already labeled, indicate the name of the column. If there is no header, write its position begining by 0.')
parser.add_argument('--header', help = 'If indicated, the file has a header row', action = 'store_true')

fileName = parser.parse_args().filename
learntWeights = numpy.loadtxt(parser.parse_args().weights)
learntBiases = numpy.loadtxt(parser.parse_args().biases)
labelColumn = parser.parse_args().labelColumn
header = parser.parse_args().header
numLabels = learntWeights.shape[1]

if header:
	dataset = pandas.read_csv(fileName)
else:
	dataset = pandas.read_csv(fileName, header = None)
	dataset.columns = map(str, range(dataset.shape[1])) # New header of ints as str

if labelColumn:
	labels = dataset[labelColumn]
	dataset.drop([labelColumn], axis = 1, inplace = True)

with open(TMP_VARIABLES_MAP, 'r') as variablesMapFile:
	variablesReader = csv.reader(variablesMapFile, delimiter = ',')
	for row in variablesReader:
		variablesOrder = list(row)

unusedVariables = list(set(dataset.columns) - set(variablesOrder))
dataset.drop(unusedVariables, axis = 1, inplace = True)
dataset = dataset[variablesOrder]

numFeatures = dataset.shape[1]
numSamples = dataset.shape[0]

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Define tensorflow

samples = tensorflow.placeholder(tensorflow.float32, [None, numFeatures])
targets = tensorflow.placeholder(tensorflow.int64, None)
weights = tensorflow.placeholder(tensorflow.float32, [numFeatures, numLabels])
biases = tensorflow.placeholder(tensorflow.float32, [numLabels])
predictions = tensorflow.matmul(samples, weights) + biases

if labelColumn:
	predicted = tensorflow.placeholder(tensorflow.int64, None)
	correctPrediction = tensorflow.equal(predicted, targets)
	accuracy = tensorflow.reduce_mean(tensorflow.cast(correctPrediction, tensorflow.float32))

sess = tensorflow.Session()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Predictions

classification = predictions.eval(session = sess, feed_dict = {samples: dataset, weights : learntWeights, biases: learntBiases})
prediction = sess.run(tensorflow.argmax(classification, 1))

if labelColumn:
	print 'Accuracy on the set: ', sess.run(accuracy, feed_dict = {predicted: prediction, targets : labels})
	dataset[labelColumn] = labels

dataset['prediction'] = prediction
dataset.to_csv(fileName + '.predicted', encoding = 'utf-8', index = False)

print 'Predictions completed! A new file called', fileName + '.predicted has been created where a new column called "prediction" holds the estimated label for each sample.'
