import os

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

TMP_VARIABLES_MAP = '/home/brayan/Taverna/tmpVariablesMap'
TMP_HEADER = '/home/brayan/Taverna/tmpHeaderRec'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Main program

try:
    os.remove(TMP_VARIABLES_MAP)
    os.remove(TMP_HEADER)
except OSError:
    pass