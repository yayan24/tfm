import os

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constant definition

REPOSEARCH_FILE = '/home/brayan/Taverna/repoSearch-results'
CURRENT_RESULT_FILE = '/home/brayan/Taverna/repoSearch-current'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Main program

try:
    os.remove(REPOSEARCH_FILE)
    os.remove(CURRENT_RESULT_FILE)
except OSError:
    pass