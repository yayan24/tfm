import sys, os, requests, urllib, json, re

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constants definition

DATASETS_FOLDER = '/home/brayan/Taverna/datasets'
CURRENT_RESULT_FILE = '/home/brayan/Taverna/repoSearch-current'

DATAVERSE_API_KEY = 'aa7b80fd-db9d-4f10-a33c-8f0879d0160d'
DATASET_API_POINT = 'https://dataverse.harvard.edu/api/datasets/:persistentId/?'
FILEACCESS_API_POINT = 'https://dataverse.harvard.edu/api/access/datafile/'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Dataset file download

currentResultFile = open(CURRENT_RESULT_FILE, 'r')
globalId = currentResultFile.read()
currentResultFile.close()

searchQuery = {'persistentId' : globalId, 'key' : DATAVERSE_API_KEY}
searchEncoded = urllib.urlencode(searchQuery)

response = requests.get(DATASET_API_POINT + searchEncoded)
responseDict = json.loads(response.text)

if len(responseDict['data']['latestVersion']['files']) == 0:
	print 'No files in this dataset...'
	print 'Showing the following result.'
	sys.exit()
	
fileData = responseDict['data']['latestVersion']['files'][0]['dataFile']

if re.match('.*pdf.*|.*various.*|.*i', fileData['contentType'].lower()) or re.match('.*x-stata.*|.*spss-*', fileData['originalFileFormat']):
	print 'This dataset includes a non-open file format (PDF, SPSS, Stata, etc)...'
	print 'Showing the following result.'
	sys.exit()
	
searchQuery = {'format' : 'original', 'key' : DATAVERSE_API_KEY}
searchEncoded = urllib.urlencode(searchQuery)

response = requests.get(FILEACCESS_API_POINT + str(fileData['id']) + '?' + searchEncoded)

if not os.path.exists(DATASETS_FOLDER):
    os.makedirs(DATASETS_FOLDER)

fileName = fileData['filename']
targetFile = open(DATASETS_FOLDER + '/' + fileName, 'w')
targetFile.truncate()
targetFile.write(response.text)
targetFile.close()

print "Datafile '" + fileName + "' donwloaded!"