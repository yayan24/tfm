import argparse, csv, pickle

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constants definition

REPOSEARCH_FILE = '/home/brayan/Taverna/repoSearch-results'
CURRENT_RESULT_FILE = '/home/brayan/Taverna/repoSearch-current'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Data definition

parser = argparse.ArgumentParser(description = 'This module reads the results send by the repository.')
parser.add_argument('keywords', help = 'List of keywords used to perform the search')
parser.add_argument('--headerFile', help = 'File with a header row that can be used to check datasets\' importance')

searchKeywords = parser.parse_args().keywords
headerFile = parser.parse_args().headerFile

resultsFile = open(REPOSEARCH_FILE, 'r')
results = pickle.load(resultsFile)

header = searchKeywords.lower().split() # In case there is no headerFile, we use search keywords

if headerFile:
	header = [word.lower() for word in next(csv.reader(open(headerFile, 'rb')), [])]

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Result reading

dataset = results[0]

print dataset['name'], '\n'
print '- Search score:', dataset['score']
print '- Global Id:', dataset['global_id']

if 'description' in dataset:
	print '- Description:'
	lenght = len(dataset['description'])
	maxLength = 100

	for i in range(lenght / maxLength):
		if i * maxLength + 2 * maxLength > lenght:
			print dataset['description'][i * maxLength : ]
		else:
			print dataset['description'][i * maxLength : i * maxLength + maxLength]	

	matches = [word for word in header if word in dataset['description'].lower()]
	print '- Matches:', len(matches) , '/', len(header)
	print '- Matched words:', ', '.join(matches)

resultsFile = open(REPOSEARCH_FILE, 'w')
resultsFile.truncate()
pickle.dump(results[1:], resultsFile)
resultsFile.close()

currentResultFile = open(CURRENT_RESULT_FILE, 'w')
currentResultFile.truncate()
currentResultFile.write(dataset['global_id'])
currentResultFile.close()