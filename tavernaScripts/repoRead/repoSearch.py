import requests, urllib, json, argparse, csv, pickle

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Constants definition

REPOSEARCH_FILE = '/home/brayan/Taverna/repoSearch-results'
DATAVERSE_API_KEY = 'aa7b80fd-db9d-4f10-a33c-8f0879d0160d'
SEARCH_API_POINT = 'https://dataverse.harvard.edu/api/search?'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Input data read

parser = argparse.ArgumentParser(description = 'This module launches the search over the Harvard Dataverse.')
parser.add_argument('keywords', help = 'List of keywords to use to perform the search')
parser.add_argument('--headerFile', help = 'File with a header row that can be used to check datasets\' importance')

searchKeywords = parser.parse_args().keywords
headerFile = parser.parse_args().headerFile

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Search

searchQuery = {'q' : searchKeywords, 'type' : 'dataset', 'show_relevance': 'true', 'per_page' : 20, 'key' : DATAVERSE_API_KEY}
searchEncoded = urllib.urlencode(searchQuery)

response = requests.get(SEARCH_API_POINT + searchEncoded)
responseDict = json.loads(response.text)

header = searchKeywords.lower().split() # In case there is no headerFile, we use search keywords

if headerFile:
	header = [word.lower() for word in next(csv.reader(open(headerFile, 'rb')), [])]
	print 'Read header:', ', '.join(header)

print responseDict['data']['total_count'], 'datasets were found!'

resultsFile = open(REPOSEARCH_FILE, 'w')
resultsFile.truncate()
pickle.dump(responseDict['data']['items'], resultsFile)
resultsFile.close()