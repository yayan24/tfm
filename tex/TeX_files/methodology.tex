\chapter{Tecnologías}
\label{cha:tecnologia}

\begin{quotation}
	\textit{En este capítulo se explican las herramientas y técnicas utilizadas en este trabajo, brevemente introducidas en la primera sección \ref{sec:intro-met}. La sección \ref{sec:python} habla sobre Python y las principales librerías utilizadas. En la sección \ref{sec:dataverse} se detalla el repositorio utilizado para obtener los datos a trabajar. La sección \ref{sec:tensorflow} detalla la librería empleada para llevar a cabo el entrenamiento de los modelos de aprendizaje automático. Finalmente, la sección \ref{sec:taverna} trata la herramienta utilizada para englobar el proyecto y facilitar su uso por parte de otros investigadores.}
\end{quotation}

\section{Introducción}
\label{sec:intro-met}

Para el desarrollo de la propuesta se ha elegido el lenguaje de programación Python, motivada dicha elección por la facilidad y rapidez que se puede conseguir en el desarrollo de aplicaciones con este lenguaje gracias a su variedad de librerías.

En cuanto al módulo de exploración del repositorio de Harvard nos hemos ayudado de la propia API que expone Dataverse. Finalmente, se ha decidido utilizar el \textit{framework} Tensorflow de Google para construir el modelo de aprendizaje y el recomendador por la posibilidad de generalizar flujos de trabajo con datos en Python.

Todo esto, además, es construido utilizando Taverna: un sistema de creación de flujos de trabajo científicos. De esta manera, la herramienta propuesta se puede liberar fácilmente a la comunidad y esta puede reutilizarla a través de esta aplicación.

\section{Python}
\label{sec:python}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1.0\textwidth]{img/methodology/python}
	\caption{Imagen propiedad de Python Software Foundation \cite{PythonLogo}.}
	\label{fig:pythonlogo}
\end{figure}

El lenguaje de programación Python es un lenguaje multiplataforma con una licencia de código abierto. Debido a la gran cantidad de módulos y librerías que extienden su funcionalidad, el sistema propuesto se ha desarrollado en este lenguaje en su versión 2.7.6. A continuación se describen las principales extensiones utilizadas para la construcción del proyecto:

\begin{itemize}
	\item \textbf{argparse:} este es un módulo que facilita la creación de interfaces a la hora de pasar argumentos a la aplicación a través de la línea de comandos. Así es posible indicar argumentos opcionales u obligatorios, entre otras opciones como darles valores por defecto.
	\\
	\item \textbf{numpy:} paquete fundamental para el cálculo científico que nos proporciona objetos que actúan como vectores multidimensionales, generadores de números aleatorios y otras posibilidades.
	\\
	\item \textbf{pandas:} librería que amplía las capacidades de \textit{numpy} con respecto a crear contenedores ofreciendo gran número de funcionalidades para trabajar con conjuntos de datos. Incluye también múltiples herramientas para el análisis de dichos datos.
	\\
	\item \textbf{matplotlib:} una de los principales paquetes para la creación de figuras en 2D interactivas y con numerosas opciones de configuración.
\end{itemize}

\section{Dataverse}
\label{sec:dataverse}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{img/methodology/dataverse}
	\caption{Imagen propiedad de Dataverse \cite{DataverseLogo}.}
	\label{fig:dataverselogo}
\end{figure}

Dataverse es una herramienta web \textit{open source} que sirve para crear un repositorio de datos de investigación. Dicho repositorio permite compartir conjuntos de datos, citarlos, explorarlos e incluso analizarlos. La herramienta es desarrollada en Harvard, en el \textit{Institute for Quantitative Social Science (IQSS)}, junto con una comunidad colaboradora.

Para su uso por parte de desarrolladores tiene habilitada una API accesible desde varios puntos \cite{DataverseAPI}. Por un lado tiene un acceso SWORD (\textit{Simple Web-service Offering Repository Deposit}) que permite acceder a Dataverse para subir, consultar o modificar conjuntos de datos. Por otro lado, es posible lanzar consultas directamente sobre el sitio aprovechando su API REST.

En el sistema desarrollado con este trabajo se ha aprovechado la API REST para trabajar directamente con el repositorio puesto que todo lo que se ha necesitado hacer es consultar y descargar datos, siendo ambas funciones posibles por esta vía.

\subsection*{Consulta del repositorio}

La consulta del repositorio es muy sencilla. Dado que Dataverse es una herramienta web, tendremos un punto de acceso a la API como puede ser \url{https://demo.dataverse.org/api/}. Sobre dicha dirección podemos lanzar una consulta añadiendo el término $search?q=breast+cancer$ para obtener un JSON con el listado de conjuntos que estén publicados en el repositorio y coincidan con los términos de búsqueda \textit{breast} y \textit{cancer}.

Es posible indicar otros parámetros en la búsqueda para filtrar, por ejemplo, el tipo de resultado devuelto (colecciones de conjuntos de datos, conjuntos de datos, ficheros) o fechas de publicación.

\subsection*{Consulta de un conjunto de datos}

Para ver más información de un conjunto de datos, la API proporciona otro acceso al que podemos enviar una petición indicando el DOI del conjunto. Así, si a la dirección \url{https://demo.dataverse.org/api/} le añadimos $datasets/:persistentId/?persistentId=DOI$ obtenemos un JSON con la información adicional del conjunto que coincida con dicho DOI.

\subsection*{Descarga de un fichero}

El último punto de la API aprovechado en este proyecto es el de acceso a ficheros. Al consultar un conjunto de datos específico se obtienen identificadores únicos de sus ficheros. Aprovechando ese identificador, se puede lanzar una consulta como $access/datafile/IDENTIFICADOR$ que nos devolverá el contenido del fichero que posea dicho identificador.

Dado que Dataverse acepta multitud de tipos de ficheros en sus conjuntos de datos, existe la posibilidad de que el fichero que se desee descargar no tenga un formato abierto reutilizable. Esto ocurriría con formatos como el PDF, SPSS, Stata... En este proyecto se ha acotado el rango de ficheros posibles a descargar únicamente a los CSV o tipo texto pues son formatos abiertos que se pueden reaprovechar fácilmente con una herramienta automática como la propuesta.

Para poder realizar cualquiera de estas peticiones al Dataverse es necesario indicar en la consulta un token autenticador. Este token es único para cada usuario y lo proporcionará la organización que administre el Dataverse consultado. En el caso propuesto será Harvard al utilizar su sitio Dataverse.

\newpage

\section{Tensorflow}
\label{sec:tensorflow}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{img/methodology/tensorflow}
	\caption{Imagen propiedad de Google \cite{TensorflowLogo}.}
	\label{fig:tensorflowlogo}
\end{figure}

Tensorflow \cite{Tensorflow2015} es una librería de código abierto de cálculo computacional basada en la definición de grafos con flujos de tensores. Inicialmente fue desarrollada por investigadores del \textit{Google Brain Team} para usarla principalmente en proyectos de aprendizaje automático y \textit{deep learning}. Sin embargo, es posible su aplicación en un mayor marco de dominios.

La librería tiene desarrollada una interfaz para Python pero es posible usarla en otros lenguajes como C++ o Go. Una de sus mayores fortalezas reside en su gran portabilidad y la capacidad de aprovechar al máximo el hardware del que dispone la máquina donde se ejecuta. Por ello, es perfectamente posible llevar un código de una máquina donde corre en CPU a otra para que se ejecute en una GPU. Incluso puede trasladarse a plataformas móviles y todo sin tener que modificar el código.

Su uso en este proyecto viene fuertemente influenciado por la capacidad de definir un flujo de datos de forma genérica. Así, el sistema propuesto es capaz de trabajar con conjuntos de datos diversos: distinto número de características, distintos tipos de datos numéricos y diferentes tamaños de muestras. Además, por su portabilidad es ideal para ser usada en distintos sectores científicos donde puedan disponer de diferentes máquinas: pequeños servidores con una sola CPU, máquinas con varias CPUs o incluso con GPUs.

Además, Tensorflow dispone de una herramienta gráfica donde poder visualizar los grafos definidos programacionalmente llamada Tensorboard. Un ejemplo de grafo se presenta en la figura \ref{fig:tensorboard}. Adicionalmente, esta herramienta permite visualizar la evolución de variables como el ratio de error durante el aprendizaje.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.73\textwidth]{img/methodology/tensorboard}
	\caption{Ejemplo de grafo definido en Tensorflow en la visualización de Tensorboard. Imagen propiedad de Google \cite{Tensorboard}.}
	\label{fig:tensorboard}
\end{figure}

En el desarrollo del proyecto se ha utilizado la versión 0.11.0, con licencia Apache 2.0, bajo una implementación para Ubuntu 14.04 con CUDA 8.0 y cuDNN 5.1.

\section{Taverna}
\label{sec:taverna}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.3\textwidth]{img/methodology/taverna}
	\caption{Imagen propiedad de Apache \cite{TavernaLogo}.}
	\label{fig:tavernalogo}
\end{figure}

Taverna es un sistema de administración de flujos de trabajo o \textit{workflows} que se ejecuta en Java, creado inicialmente por myGrid pero actualmente es ya un proyecto \textit{Apache Incubator}. Este sistema consiste en un conjunto de herramientas con las que un usuario puede crear, buscar y ejecutar flujos de trabajo científicos donde se definen procesos e interacciones entre módulos o incluso el usuario. Se utiliza principalmente en ámbitos como la bioinformática, la química o la astronomía.

El uso de Taverna para desarrollo consiste en la creación de un flujo de trabajo utilizando su aplicación de escritorio. En esta aplicación se pueden incorporar módulos llamados servicios con código en R o Java, llamadas a servicios web usando WSDL o interfaces API REST, manejo de hojas de datos o incluso llamadas de sistema. También incluye algunos servicios conocidos y usados en el ámbito de la biología como BioMart y BioMoby.

El resultado es un diagrama donde se especifican unos datos de entrada y se obtienen una salidas, tal y como se muestra en la figura \ref{fig:tavernaworkflow}. Su uso en esta aplicación nos aporta la posibilidad de definir todos los módulos en Python, usando su servicio llamado \textit{Tool}, y que su interacción con el usuario sea a través de estos flujos. Además, a la hora de liberar la herramienta tan solo es necesario el fichero con la definición del flujo que se genera desde Taverna. Este puede ser depositado en un repositorio como myExperiment, del que hablábamos en la sección \ref{sec:estado-del-arte} del estado del arte, pues en este sitio se comparten este tipo de diagramas.

\newpage

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.3\textwidth]{img/methodology/tavernaworkflow}
	\caption{Ejemplo de \textit{workflow} científico con Taverna. Imagen extraída de Apache \cite{TavernaWorkflow}.}
	\label{fig:tavernaworkflow}
\end{figure}

En este proyecto hemos utilizado Taverna Workbench en su versión 2.5 para sistemas linux Debian, liberado bajo una licencia LGPL 2.1.