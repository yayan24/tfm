\chapter{Introducción}
\label{cha:introduccion}

\begin{quotation}
	\textit{En este capítulo se da una breve introducción al trabajo en la sección \ref{sec:intro-intro}. Tras ello, en la sección \ref{sec:motivacion} se presenta la motivación del mismo. En la sección \ref{sec:estado-del-arte} se verá el estado actual de la materia para continuar en la sección \ref{sec:propuesta} definiendo la propuesta del trabajo. Finalmente, en la sección \ref{sec:objetivos} se listan los principales objetivos que se intentan conseguir. La sección \ref{sec:estructura} describe la estructura del resto del documento.}
\end{quotation}

\section{Introducción}
\label{sec:intro-intro}

En este trabajo final de máster hemos desarrollado un ecosistema de programas con los que un investigador - como podría ser un investigador en informática médica - es capaz de explorar repositorios de datos abiertos, descargar conjuntos de datos, realizar un \textit{profiling} o preprocesamiento de los mismos, así como construir mediante el uso de aprendizaje automático un sistema recomendador para después poder reutilizarlo  con otros datos, ya sean abiertos o privados.

Creemos que este sistema puede ser de utilidad a la comunidad científica e investigadora de forma que puedan sacar un mayor provecho del gran potencial de los datos abiertos. Se busca con este proyecto fomentar una cultura de trabajo entre los investigadores de reutilizar y a su vez abrir los datos de investigación. Además de indagar en las posibilidades que ofrece la reutilización de datos abiertos de investigación.

\section{Motivación}
\label{sec:motivacion}

Este documento se presenta como resultado de la investigación y el desarrollo realizado al final del Máster en Ingeniería Informática, con especialidad en \textit{Tecnologías informáticas para la innovación}, cursado entre los años 2015 y 2017 en la \textit{Universidad de Alicante (España)}. La principal motivación de este proyecto viene desde un gran interés personal en los datos abiertos, la inteligencia artificial y su aplicabilidad en otros campos como la salud, además del deseo de trabajar en un proyecto innovador donde aplicar los conocimientos adquiridos en la especialidad cursada.

Supongamos que somos unos investigadores en informática médica. Como investigadores estamos desarrollando un trabajo con datos clínicos (por ejemplo resultados de pruebas médicas). El objetivo principal de dicho trabajo es el de extraer conocimiento de estos datos. Por ejemplo, sería muy útil disponer de un sistema de recomendación que ayude a detectar ciertas enfermedades como el cáncer.

En estos casos sería de gran utilidad poder encontrar trabajos donde se hayan empleado datos de una naturaleza similar. Hoy en día se suele abordar la resolución de esta situación mediante la búsqueda y la lectura de bibliografía especializada. No obstante, a través de este método tradicional de publicación solamente se comparten conclusiones o resultados. En el supuesto que estamos trabajando lo que necesitamos son datos.

Por otro lado existen los datos abiertos que pueden constituir una fuente de información que dirijan una investigación o puedan apoyarla. La reutilización de estos datos serían de utilidad para nuestro supuesto ya que complementarían su trabajo.

Según concluye Fernanda Peset en su presentación sobre datos abiertos en investigación durante el Encuentro de Datos Abiertos en la Universidad de Alicante 2015 \cite{Peset2015}, es necesario \textit{"desarrollar y utilizar nuevas herramientas de software para automatizar y simplificar la creación y \textbf{explotación} de conjuntos de datos..."}. Este hecho nos alienta a desarrollar una herramienta capaz de encontrar esos datos, explorarlos y utilizarlos en la construcción, en este caso, de un sistema recomendador.

\section{Estado del arte}
\label{sec:estado-del-arte}

Desde hace unos años ya se reconoce que los datos son el núcleo de la economía del futuro y ya es frecuente escuchar que los datos son el nuevo petróleo o el nuevo oro. En reconocimiento de ello, en 2013 los líderes del G8 firmaron lo que se conoce como \textbf{La Carta de los Datos Abiertos} \cite{OpenChartG82013} para manifestar su deseo de potenciar los datos abiertos.

Destacan desde el G8 en dicha carta el principio de que los datos deben ser abiertos por defecto debido a su alto valor para la sociedad y la economía, comprometiéndose a reorientar sus políticas hacia el cumplimiento de este principio. Además, hacen hincapié en que los datos deben ser publicados en formatos reusables por todos: personas y máquinas, de modo que se pueda estimular la creatividad y la innovación.

Estos deseos coinciden con lo que el comunicado \verb|COM(2014) 442 final| \\ 
\verb|2.7.2014| \textbf{"Hacia una economía de los datos próspera"} de la Comisión Europea \cite{ComEu2014} expresa: vivimos una nueva revolución que está impulsada por los datos digitales. Todas nuestras actividades diarias, al igual que los procesos industriales y la investigación conllevan a la recolección y el procesamiento de datos para crear nuevos productos, servicios y metodologías científicas (como el \textit{Data Driven Research}).

Por ello, desde la Unión Europea se impulsan mandatos y obligaciones a los investigadores que participan en sus proyectos del marco H2020 para que publiquen sus datos de forma abierta, como se indica en la guía de publicación del programa \cite{H2020Guideline}. En apoyo a esta política nace el proyecto \textbf{OpenAIRE} \cite{OpenAIRE} que pretende ser un mecanismo para hacerla cumplir. Este portal consiste en una red de repositorios de datos de acceso abierto, archivos y revistas que apoyan también las políticas del acceso abierto a los datos de investigación.

Para cumplir esta función, el CERN colabora con OpenAIRE y desarrolla \textbf{Zenodo} \cite{Zenodo}, un gran repositorio o librería donde los investigadores pueden publicar sus trabajos de forma abierta, admitiendo ficheros en toda clase de formatos y tamaños. Desde el propio sitio se motiva a publicar todo documento o material relacionado con una investigación en favor de la replicabilidad y la credibilidad.

El material publicado en Zenodo se comparte con otro proyecto: \textbf{DataCite} \cite{DataCite}. Esta es una organización sin ánimo de lucro que provee de identificadores persitentes (DOIs) a datos de investigación. Su principal objetivo es ayudar a la comunidad científica a localizar, identificar y proporcionar confianza a la hora de citar datos de investigación. Su uso también está apoyado desde la guía de publicación del programa H2020.

Además, DataCite cuenta con un registro de repositorios de datos de investigación llamado \textbf{re3data} \cite{re3data}. Este registro es global y cubre tanto repositorios genéricos como específicos a disciplinas académicas, siendo entonces también un sitio que promueve la cultura de compartir datos en investigación.

Entre otros repositorios de publicación de datos abiertos para investigadores cabe destacar \textbf{figshare} \cite{Figshare}. Este es un repositorio que permite a los usuarios subir ficheros en cualquier formato, los cuales identifica con los DOI que provee DataCite, e incluso estos pueden tener previsualizaciones desde el propio explorador. Su meta es que cualquier resultado de una investigación pueda ser divulgado de una forma que el actual sistema académico de publicación no permite.

Una alternativa a Figshare es el repositorio \textbf{Dryad} \cite{Dryad}. Dryad es un sitio creado por una organización sin ánimo de lucro que pretende hacer público todos los datos que apoyan las publicaciones científicas de forma que se pueda reutilizar y citar.

De igual manera, la \textit{Open Knowledge Foundation} también cuenta con una plataforma para la publicación de datos abiertos conocida como \textbf{Datahub} \cite{Datahub}. Sin embargo, esta web consiste en un repositorio más general y no es específico para la apertura de datos abiertos de investigación, aunque también son admitidos.

Como se puede ver, existe una variedad de repositorios que persiguen unos objetivos más o menos similares: ser fuente donde los investigadores puedan dar acceso abierto a los datos de su investigación de manera que sean reutilizables. Sin embargo, cada uno lo ha desarrollado de una forma diferente por lo que su uso varía. Desde Harvard se desarrolla el proyecto \textbf{Dataverse} \cite{Dataverse} para proporcionar una herramienta de creación de repositorios de forma que se estandaricen.

Dataverse es una plataforma web de código abierto con la que cualquier organización puede crear un repositorio donde compartir, almacenar, citar, explorar y analizar datos que puede publicar cualquier autor, recibiendo este todo el crédito a su trabajo. Uno de los objetivos de Dataverse es facilitar también el acceso a los datos a desarrolladores por lo que cuenta con una interfaz API e incluso librerías programacionales en lenguajes como Python, R y Java para trabajar con sus repositorios.

En consonancia con estos trabajos que bogan en dirección a la apertura de los datos en ciencia, se pueden encontrar sitios como \textbf{MyExperiment} \cite{MyExperiment} y \textbf{ResearchObject} \cite{ResearchObject}. Estos consisten en repositorios de experimentos científicos. Ya no solo se publican datos y material relacionado con un proyecto de investigación sino que se admiten esquemas de cómo se han trabajado los datos o cómo se ha conducido la investigación. Con esto apuestan por la replicabilidad de los proyectos.

\section{Propuesta}
\label{sec:propuesta}

La tendencia actual es el desarrollo de sitios web donde poder almacenar y organizar los datos que los investigadores generan o utilizan en sus proyectos. Con ello se pretende que todo aquel que lo desee pueda reutilizarlos. Sin embargo, no hemos encontrado herramientas con las que poder explorar estos repositorios de tal manera que se pueda apoyar el desarrollo de un proyecto de investigación.. La mayoría de herramientas que se están desarrollando se centran en el punto del almacenamiento pero no van más allá (consultar el listado en \textit{Opening Science} para ver más herramientas de este tipo \cite{Tools}), es decir, no se consideran mecanismos que faciliten la reutilización de datos abiertos de investigación.

Desde el comunicado de la Comisión Europea \textit{"Hacia una economía de los datos próspera"} \cite{ComEu2014} en la sección 4.2.1, en el punto \textit{2 - Herramientas y métodos de tratamiento de datos}, se dice que:

\begin{quotation}
\textit{Con el fin de fomentar la I+D en la inteligencia de negocio, los procesos de apoyo a la decisión y los sistemas de ayuda a las pymes y los emprededores web, H2020 aborda las analíticas de datos descriptivas y predictivas, la visualización de datos, la inteligencia artificial y las herramientas software y algoritmos de toma de decisiones.}
\end{quotation}

\newpage

Así pues, la Comisión Europea reconoce la necesidad de herramientas que no solo apoyen la recolección de datos para que tengan un libre acceso sino que también son necesarias aquellas que permitan trabajarlos para apoyar la toma de decisiones.

Por ello, nuestra propuesta consiste en la definición del proceso de reutilización y el desarrollo de un sistema capaz de explorar un repositorio de datos abiertos del cual seleccionar conjuntos de datos y poder explotarlos. Debido a que este proyecto consiste en un trabajo de fin de máster, se ha acotado el universo de repositorios a explorar en uno solo. El repositorio que nuestro sistema explora es el administrado por Harvard a través del uso de un Dataverse, accesible en \url{https://dataverse.harvard.edu/}, aunque podría cambiarse por cualquier otro Dataverse con muy poca reconfiguración de la herramienta.

Con este sistema también es posible realizar un \textit{profiling} o exploración de los datos que contenga un conjunto descargado, así como preprocesarlos con técnicas comunes de minería de datos. De esta manera abordamos un punto muy importante nombrado en la cita del comunicado anterior: el sistema realiza analítica descriptiva y permite tener una visualización de los datos.

Así mismo, la herramienta propuesta es capaz de construir un modelo de aprendizaje automático a partir de los datos abiertos que se le suministren y exportarlo de forma que se pueda reaprovechar mediante su aplicación a otros datos de similar naturaleza. De esta manera, el sistema cuenta además con la opción de reutilizar el modelo para lanzar recomendaciones o etiquetar un conjunto de muestras dado, sea privado u obtenido de un repositorio de datos abiertos. Con esta funcionalidad queremos cubrir la necesidad reconocida por la Comisión Europea de crear herramientas software que realicen análisis predictivo y para ello utilizamos algoritmos de inteligencia artificial.

Además, para facilitar la apertura y utilización de la herramienta por la comunidad investigadora, esta se desarrollará con tecnologías multiplataforma y abiertas que permitan su uso por parte de cualquier persona de manera sencilla. Como resultado, tenemos un sistema de apoyo a la toma de decisiones abierto y reutilizable por parte de usuarios de cualquier ámbito, pues solo dependerá de la disciplina de aplicación de los datos usados.

\section{Objetivos}
\label{sec:objetivos}

El objetivo principal del proyecto es el de definir un proceso donde se reutilicen datos abiertos para extraer el conocimiento necesario en la construcción de un sistema recomendador que pueda apoyar un proceso de toma de decisiones. Para conseguir dicho objetivo, se construye una herramienta y se definen unos objetivos secundarios para la misma:

\begin{enumerate}
	\item Ser capaces de explorar un repositorio de datos abiertos.
	\item Proporcionar herramientas para la visualización de datos.
	\item Implementar técnicas que preparen los datos para un proyecto de minería.
	\item Construir un modelo de aprendizaje automático que sea genérico y pueda aceptar conjuntos de datos abiertos con distintas características.
	\item Instrumentalizar el modelo de forma que se pueda reutilizar.
	\item Poder realizar recomendaciones con el modelo exportado sobre otros conjuntos de datos.
	\item Proporcionar su uso a través de tecnologías multiplataforma y abiertas.
	\item Realizar procesados de datos lo más rápidos y eficientes que sea posible.
\end{enumerate}

\section{Estructura}
\label{sec:estructura}

En este capítulo se ha introducido la materia de este trabajo de fin de máster. Se han presentado trabajos relacionados para describir el estado actual de la misma y así poder dar pie a nuestra propuesta, junto con sus objetivos. En el capítulo \ref{cha:tecnologia} veremos las tecnologías utilizadas y la metodología empleada para desarrollar el proyecto. El capítulo \ref{cha:propuesta} describe el sistema a través de un caso de uso. Finalmente, en el capítulo \ref{cha:conclusiones} se discuten las conclusiones obtenidas con la elaboración de este trabajo de fin de máster.