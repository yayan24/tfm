\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introducci\IeC {\'o}n}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Motivaci\IeC {\'o}n}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Estado del arte}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Propuesta}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Objetivos}{7}{section.1.5}
\contentsline {section}{\numberline {1.6}Estructura}{7}{section.1.6}
\contentsline {chapter}{\numberline {2}Tecnolog\IeC {\'\i }as}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Introducci\IeC {\'o}n}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Python}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Dataverse}{11}{section.2.3}
\contentsline {section}{\numberline {2.4}Tensorflow}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}Taverna}{15}{section.2.5}
\contentsline {chapter}{\numberline {3}Propuesta}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Introducci\IeC {\'o}n}{17}{section.3.1}
\contentsline {section}{\numberline {3.2}Arquitectura de la soluci\IeC {\'o}n}{18}{section.3.2}
\contentsline {section}{\numberline {3.3}Exploraci\IeC {\'o}n del repositorio}{19}{section.3.3}
\contentsline {section}{\numberline {3.4}Visualizado y preprocesado de los datos}{26}{section.3.4}
\contentsline {section}{\numberline {3.5}Aprendizaje autom\IeC {\'a}tico}{44}{section.3.5}
\contentsline {section}{\numberline {3.6}Recomendaci\IeC {\'o}n sobre datos}{51}{section.3.6}
\contentsline {section}{\numberline {3.7}Ejecuci\IeC {\'o}n}{54}{section.3.7}
\contentsline {chapter}{\numberline {4}Conclusiones}{57}{chapter.4}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{61}{chapter.4}
